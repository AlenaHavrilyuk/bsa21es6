import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if(!fighter){
    return "chose fighter";
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const {  name, attack, health, defense } = fighter;

  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter info',
  }
  )

  const attackElement = createElement({
    tagName: 'div',
    className: 'attack',
  });
  attackElement.innerText = `attack = ${attack}`;

  const fighterNameElement = createElement({
    tagName: 'div',
    className: 'name',
  });
  fighterNameElement.innerText = `name = ${name}`;

  const healthElement = createElement({
    tagName: 'div',
    className: 'health',
  });
  healthElement.innerText = `health = ${health}`;

  const defenseElement = createElement({
    tagName: 'div',
    className: 'defense',
  });
  defenseElement.innerText = `defense = ${defense}`;

  fighterInfo.append(fighterNameElement, attackElement, healthElement, defenseElement);

  fighterElement.append(fighterInfo);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
